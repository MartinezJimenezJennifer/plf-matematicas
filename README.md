# Mapas conceptuales "Matemáticas"
## *Conjuntos, Aplicaciones y funciones (2002)*
```plantuml
@startmindmap
title Cuadro Sinóptico\n**"Conjuntos, Aplicaciones y funciones"**
footer
Elaborado por: **Martinez Jimenez Jennifer**
endfooter
<style>
mindmapDiagram{
    BackGroundColor  	#D3D3D3
}
 node {
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #black
  LineThickness 1.5
  BackgroundColor #SKYBLUE
  RoundCorner 50
  MaximumWidth 300
 }
 arrow{
  LineColor #DEEPPINK
 }
 title{
  FontSize 20
 }
 footer{
  FontSize 18
 }
</style>

@endmindmap
```
Informacion tomada de: 
[Temas 6 y 7: Conjuntos, Aplicaciones y funciones](https://canal.uned.es/video/5a6f1b77b1111f15098b4609)

## *Funciones (2010)*
```plantuml
@startmindmap
title Cuadro Sinóptico\n**"Funciones"**
footer
Elaborado por: **Martinez Jimenez Jennifer**
endfooter
<style>
mindmapDiagram{
    BackGroundColor  	#D3D3D3
}
 node {
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #black
  LineThickness 1.5
  BackgroundColor #SKYBLUE
  RoundCorner 50
  MaximumWidth 300
 }
 arrow{
  LineColor #DEEPPINK
 }
 title{
  FontSize 20
 }
 footer{
  FontSize 18
 }
</style>

@endmindmap
```
Informacion tomada de: 
[Funciones](https://canal.uned.es/video/5a6f2d09b1111f21778b457f)

## *La matemática del computador (2002)*
```plantuml
@startmindmap
title Cuadro Sinóptico\n**"La matemática del computador"**
footer
Elaborado por: **Martinez Jimenez Jennifer**
endfooter
<style>
mindmapDiagram{
    BackGroundColor  	#D3D3D3
}
 node {
  Padding 10
  Margin 8
  HorizontalAlignment center
  LineColor #black
  LineThickness 1.5
  BackgroundColor #SKYBLUE
  RoundCorner 50
  MaximumWidth 300
 }
 arrow{
  LineColor #DEEPPINK
 }
 title{
  FontSize 20
 }
 footer{
  FontSize 18
 }
</style>

@endmindmap
```
Informacion tomada de: 
[La matemática del computador. El examen final](https://canal.uned.es/video/5a6f1b76b1111f15098b45fa),[Tema 12: Las matemáticas de los computadores. El examen final](https://canal.uned.es/video/5a6f1b81b1111f15098b4640)


